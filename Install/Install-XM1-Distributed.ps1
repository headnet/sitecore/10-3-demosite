# The Prefix that will be used on SOLR, Website and Database instances.
$Prefix = "XM1"
# The Password for the Sitecore Admin User. This password is required.
$SitecoreAdminPassword = ""
# The root folder with the license file, deployment config files and WDP packages.
$SCInstallRoot = "C:\ResourceFiles"
# Alternate root folder to install websites. If left on default [systemdrive]:\inetpub\wwwroot\ will be used.
$SitePhysicalRoot = ""
# The DNS name or IP of the Solr computer.
$SolrComputerName = ""
# The DNS name of the Content Management computer.
$CMComputerName = ""
# The DNS name of the Content Delivery computer.
$CDComputerName = ""
# The DNS name of the Identity Server computer.
$IdentityServerComputerName = ""
# The name for the Sitecore Content Management Server.
$SitecoreContentManagementSitename = "$Prefix.cm"
# The name for the Sitecore Content Delivery server.
$SitecoreContentDeliverySitename = "$Prefix.cd"
# Identity Server site name
$IdentityServerSiteName = "$prefix.identityserver"
# The Path to the license file
$LicenseFile = "$SCInstallRoot\license.xml"
# The URL of the Solr Server
$SolrUrl = "https://solr_host:8983/solr"
# The Folder that Solr has been installed in.
$SolrRoot = "C:\Solr-8.11.2"
# The Name of the Solr Service.
$SolrService = "Solr-8.11.2"
# The DNS name or IP of the SQL Instance.
$SqlServer = ""
# A SQL user with sysadmin privileges.
$SqlAdminUser = "sa"
# The password for $SQLAdminUser.
$SqlAdminPassword = "12345"
# The path to the Sitecore Content Management Package to Deploy
$SiteCoreContentManagementPackage = (Get-ChildItem "$SCInstallRoot\Sitecore * rev. * (XM) (OnPrem)_cm.*scwdp.zip").FullName
# The path to the Sitecore Content Delivery Package to Deploy
$SitecoreContentDeliveryPackage = (Get-ChildItem "$SCInstallRoot\Sitecore * rev. * (XM) (OnPrem)_cd.*scwdp.zip").FullName
# The path to the Identity Server Package to Deploy.
$IdentityServerPackage = (Get-ChildItem "$SCInstallRoot\Sitecore.IdentityServer * rev. * (OnPrem)_identityserver.*scwdp.zip").FullName
# The Identity Server password recovery URL, this should be the URL of the CM Instance
$PasswordRecoveryUrl = "https://$CMComputerName"
# The URL of the Identity Authority
$SitecoreIdentityAuthority = "https://$IdentityServerComputerName"
#The random string key used for establishing connection with IdentityService
$ClientSecret = "SIF-Default"
#adding allowed CORS Origins, urls of all clients should be added here
$allowedCorsOrigins = "https://$CMComputerName"
# The parameter for the installing delta WDP packages.
$Update = $false
# The elastic pool name for deploy databases from the SQL Azure.
$DeployToElasticPoolName = ""

#Getting Hosts User credentials:
Write-Host "Hosts user name: "
$HostsUserName = Read-Host
Write-Host "Hosts user password: "
$HostsUserPassword = Read-Host
Clear-Host

# Install XM1 via combined partials file.
$DistributedDeploymentParams = @{
    Path = "$SCInstallRoot\XM1-Distributed.json"
    SitecoreAdminPassword = $SitecoreAdminPassword
    ComputerUserName = $HostsUserName
    ComputerPassword = $HostsUserPassword
    SitecoreSolrComputerName = $SolrComputerName
    SitecoreSolrUserName = $HostsUserName
    SitecoreSolrPassword = $HostsUserPassword
    SitecoreCMComputerName = $CMComputerName
    SitecoreCDComputerName = $CDComputerName
    IdentityServerComputerName = $IdentityServerComputerName
    LicenseFile = $LicenseFile
    IdentityServerSiteName = $IdentityServerSiteName
    Prefix = $Prefix
    SitecoreContentManagementSitename = $SitecoreContentManagementSitename
    SitecoreContentDeliverySitename = $SitecoreContentDeliverySitename
    SitecoreIdentityAuthority = $SitecoreIdentityAuthority
    SqlAdminUser = $SqlAdminUser
    SqlAdminPassword = $SqlAdminPassword
    SqlServer = $SqlServer
    SolrUrl = $SolrUrl
    SolrRoot = $SolrRoot
    SolrService = $SolrService
    SiteCoreContentManagementPackage = $SiteCoreContentManagementPackage
    SitecoreContentDeliveryPackage = $SitecoreContentDeliveryPackage
    IdentityServerPackage = $IdentityServerPackage
    PasswordRecoveryUrl = $PasswordRecoveryUrl
    ClientSecret = $ClientSecret
    AllowedCorsOrigins = $AllowedCorsOrigins
    SitePhysicalRoot = $SitePhysicalRoot
    Update = $Update
    DeployToElasticPoolName = $DeployToElasticPoolName
}

Push-Location $SCInstallRoot

Install-SitecoreConfiguration @DistributedDeploymentParams *>&1 | Tee-Object XM1-Distributed.log

Pop-Location

# SIG # Begin signature block
# MIIl4wYJKoZIhvcNAQcCoIIl1DCCJdACAQExDzANBglghkgBZQMEAgEFADB5Bgor
# BgEEAYI3AgEEoGswaTA0BgorBgEEAYI3AgEeMCYCAwEAAAQQH8w7YFlLCE63JNLG
# KX7zUQIBAAIBAAIBAAIBAAIBADAxMA0GCWCGSAFlAwQCAQUABCC+AS7ftYMRR9kK
# Rx4J7x13FpRnKRS1abjDvNlU60juf6CCE8kwggWQMIIDeKADAgECAhAFmxtXno4h
# MuI5B72nd3VcMA0GCSqGSIb3DQEBDAUAMGIxCzAJBgNVBAYTAlVTMRUwEwYDVQQK
# EwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5jb20xITAfBgNV
# BAMTGERpZ2lDZXJ0IFRydXN0ZWQgUm9vdCBHNDAeFw0xMzA4MDExMjAwMDBaFw0z
# ODAxMTUxMjAwMDBaMGIxCzAJBgNVBAYTAlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJ
# bmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5jb20xITAfBgNVBAMTGERpZ2lDZXJ0
# IFRydXN0ZWQgUm9vdCBHNDCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIB
# AL/mkHNo3rvkXUo8MCIwaTPswqclLskhPfKK2FnC4SmnPVirdprNrnsbhA3EMB/z
# G6Q4FutWxpdtHauyefLKEdLkX9YFPFIPUh/GnhWlfr6fqVcWWVVyr2iTcMKyunWZ
# anMylNEQRBAu34LzB4TmdDttceItDBvuINXJIB1jKS3O7F5OyJP4IWGbNOsFxl7s
# Wxq868nPzaw0QF+xembud8hIqGZXV59UWI4MK7dPpzDZVu7Ke13jrclPXuU15zHL
# 2pNe3I6PgNq2kZhAkHnDeMe2scS1ahg4AxCN2NQ3pC4FfYj1gj4QkXCrVYJBMtfb
# BHMqbpEBfCFM1LyuGwN1XXhm2ToxRJozQL8I11pJpMLmqaBn3aQnvKFPObURWBf3
# JFxGj2T3wWmIdph2PVldQnaHiZdpekjw4KISG2aadMreSx7nDmOu5tTvkpI6nj3c
# AORFJYm2mkQZK37AlLTSYW3rM9nF30sEAMx9HJXDj/chsrIRt7t/8tWMcCxBYKqx
# YxhElRp2Yn72gLD76GSmM9GJB+G9t+ZDpBi4pncB4Q+UDCEdslQpJYls5Q5SUUd0
# viastkF13nqsX40/ybzTQRESW+UQUOsxxcpyFiIJ33xMdT9j7CFfxCBRa2+xq4aL
# T8LWRV+dIPyhHsXAj6KxfgommfXkaS+YHS312amyHeUbAgMBAAGjQjBAMA8GA1Ud
# EwEB/wQFMAMBAf8wDgYDVR0PAQH/BAQDAgGGMB0GA1UdDgQWBBTs1+OC0nFdZEzf
# Lmc/57qYrhwPTzANBgkqhkiG9w0BAQwFAAOCAgEAu2HZfalsvhfEkRvDoaIAjeNk
# aA9Wz3eucPn9mkqZucl4XAwMX+TmFClWCzZJXURj4K2clhhmGyMNPXnpbWvWVPjS
# PMFDQK4dUPVS/JA7u5iZaWvHwaeoaKQn3J35J64whbn2Z006Po9ZOSJTROvIXQPK
# 7VB6fWIhCoDIc2bRoAVgX+iltKevqPdtNZx8WorWojiZ83iL9E3SIAveBO6Mm0eB
# cg3AFDLvMFkuruBx8lbkapdvklBtlo1oepqyNhR6BvIkuQkRUNcIsbiJeoQjYUIp
# 5aPNoiBB19GcZNnqJqGLFNdMGbJQQXE9P01wI4YMStyB0swylIQNCAmXHE/A7msg
# dDDS4Dk0EIUhFQEI6FUy3nFJ2SgXUE3mvk3RdazQyvtBuEOlqtPDBURPLDab4vri
# RbgjU2wGb2dVf0a1TD9uKFp5JtKkqGKX0h7i7UqLvBv9R0oN32dmfrJbQdA75PQ7
# 9ARj6e/CVABRoIoqyc54zNXqhwQYs86vSYiv85KZtrPmYQ/ShQDnUBrkG5WdGaG5
# nLGbsQAe79APT0JsyQq87kP6OnGlyE0mpTX9iV28hWIdMtKgK1TtmlfB2/oQzxm3
# i0objwG2J5VT6LaJbVu8aNQj6ItRolb58KaAoNYes7wPD1N1KarqE3fk3oyBIa0H
# EEcRrYc9B9F1vM/zZn4wggawMIIEmKADAgECAhAIrUCyYNKcTJ9ezam9k67ZMA0G
# CSqGSIb3DQEBDAUAMGIxCzAJBgNVBAYTAlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJ
# bmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5jb20xITAfBgNVBAMTGERpZ2lDZXJ0
# IFRydXN0ZWQgUm9vdCBHNDAeFw0yMTA0MjkwMDAwMDBaFw0zNjA0MjgyMzU5NTla
# MGkxCzAJBgNVBAYTAlVTMRcwFQYDVQQKEw5EaWdpQ2VydCwgSW5jLjFBMD8GA1UE
# AxM4RGlnaUNlcnQgVHJ1c3RlZCBHNCBDb2RlIFNpZ25pbmcgUlNBNDA5NiBTSEEz
# ODQgMjAyMSBDQTEwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQDVtC9C
# 0CiteLdd1TlZG7GIQvUzjOs9gZdwxbvEhSYwn6SOaNhc9es0JAfhS0/TeEP0F9ce
# 2vnS1WcaUk8OoVf8iJnBkcyBAz5NcCRks43iCH00fUyAVxJrQ5qZ8sU7H/Lvy0da
# E6ZMswEgJfMQ04uy+wjwiuCdCcBlp/qYgEk1hz1RGeiQIXhFLqGfLOEYwhrMxe6T
# SXBCMo/7xuoc82VokaJNTIIRSFJo3hC9FFdd6BgTZcV/sk+FLEikVoQ11vkunKoA
# FdE3/hoGlMJ8yOobMubKwvSnowMOdKWvObarYBLj6Na59zHh3K3kGKDYwSNHR7Oh
# D26jq22YBoMbt2pnLdK9RBqSEIGPsDsJ18ebMlrC/2pgVItJwZPt4bRc4G/rJvmM
# 1bL5OBDm6s6R9b7T+2+TYTRcvJNFKIM2KmYoX7BzzosmJQayg9Rc9hUZTO1i4F4z
# 8ujo7AqnsAMrkbI2eb73rQgedaZlzLvjSFDzd5Ea/ttQokbIYViY9XwCFjyDKK05
# huzUtw1T0PhH5nUwjewwk3YUpltLXXRhTT8SkXbev1jLchApQfDVxW0mdmgRQRNY
# mtwmKwH0iU1Z23jPgUo+QEdfyYFQc4UQIyFZYIpkVMHMIRroOBl8ZhzNeDhFMJlP
# /2NPTLuqDQhTQXxYPUez+rbsjDIJAsxsPAxWEQIDAQABo4IBWTCCAVUwEgYDVR0T
# AQH/BAgwBgEB/wIBADAdBgNVHQ4EFgQUaDfg67Y7+F8Rhvv+YXsIiGX0TkIwHwYD
# VR0jBBgwFoAU7NfjgtJxXWRM3y5nP+e6mK4cD08wDgYDVR0PAQH/BAQDAgGGMBMG
# A1UdJQQMMAoGCCsGAQUFBwMDMHcGCCsGAQUFBwEBBGswaTAkBggrBgEFBQcwAYYY
# aHR0cDovL29jc3AuZGlnaWNlcnQuY29tMEEGCCsGAQUFBzAChjVodHRwOi8vY2Fj
# ZXJ0cy5kaWdpY2VydC5jb20vRGlnaUNlcnRUcnVzdGVkUm9vdEc0LmNydDBDBgNV
# HR8EPDA6MDigNqA0hjJodHRwOi8vY3JsMy5kaWdpY2VydC5jb20vRGlnaUNlcnRU
# cnVzdGVkUm9vdEc0LmNybDAcBgNVHSAEFTATMAcGBWeBDAEDMAgGBmeBDAEEATAN
# BgkqhkiG9w0BAQwFAAOCAgEAOiNEPY0Idu6PvDqZ01bgAhql+Eg08yy25nRm95Ry
# sQDKr2wwJxMSnpBEn0v9nqN8JtU3vDpdSG2V1T9J9Ce7FoFFUP2cvbaF4HZ+N3HL
# IvdaqpDP9ZNq4+sg0dVQeYiaiorBtr2hSBh+3NiAGhEZGM1hmYFW9snjdufE5Btf
# Q/g+lP92OT2e1JnPSt0o618moZVYSNUa/tcnP/2Q0XaG3RywYFzzDaju4ImhvTnh
# OE7abrs2nfvlIVNaw8rpavGiPttDuDPITzgUkpn13c5UbdldAhQfQDN8A+KVssIh
# dXNSy0bYxDQcoqVLjc1vdjcshT8azibpGL6QB7BDf5WIIIJw8MzK7/0pNVwfiThV
# 9zeKiwmhywvpMRr/LhlcOXHhvpynCgbWJme3kuZOX956rEnPLqR0kq3bPKSchh/j
# wVYbKyP/j7XqiHtwa+aguv06P0WmxOgWkVKLQcBIhEuWTatEQOON8BUozu3xGFYH
# Ki8QxAwIZDwzj64ojDzLj4gLDb879M4ee47vtevLt/B3E+bnKD+sEq6lLyJsQfmC
# XBVmzGwOysWGw/YmMwwHS6DTBwJqakAwSEs0qFEgu60bhQjiWQ1tygVQK+pKHJ6l
# /aCnHwZ05/LWUpD9r4VIIflXO7ScA+2GRfS0YW6/aOImYIbqyK+p/pQd52MbOoZW
# eE4wggd9MIIFZaADAgECAhAEe51QLiqN2yKyU0tANZXhMA0GCSqGSIb3DQEBCwUA
# MGkxCzAJBgNVBAYTAlVTMRcwFQYDVQQKEw5EaWdpQ2VydCwgSW5jLjFBMD8GA1UE
# AxM4RGlnaUNlcnQgVHJ1c3RlZCBHNCBDb2RlIFNpZ25pbmcgUlNBNDA5NiBTSEEz
# ODQgMjAyMSBDQTEwHhcNMjExMDI2MDAwMDAwWhcNMjIxMTAyMjM1OTU5WjCBgTEL
# MAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNhbiBG
# cmFuY2lzY28xGzAZBgNVBAoTElNpdGVjb3JlIFVTQSwgSW5jLjELMAkGA1UECxMC
# SVQxGzAZBgNVBAMTElNpdGVjb3JlIFVTQSwgSW5jLjCCAiIwDQYJKoZIhvcNAQEB
# BQADggIPADCCAgoCggIBALqIAka1Ql+aPWB4e8acNTR7oGt0JAAjkKxu9dhQnuh+
# DaQePslSzOXcgPgb64FJYjpHdi4s1Kt1jTgN7FFjiT3YQqDYCxv26s0EbZKSNwmN
# KlSIQDujSriirCoA6oLd8uI6dXblbPmd5xw3frkWgTc2ILqAIuo3bgHQ80AvF3pS
# 8OtXmxPNwnJ9N+PuhjG2xCDUGorEhgAtfqxb9C/6k6hZAFoufxV2ctSKWDWLAt6x
# J4eYqnr0Vn+jmfj5xttozxEdn/J3E7Qz+Sewz/i8LqE9fa6xKwNczBlZA9UKdDj4
# cGJJs/nujfq6TUTyqH8eLzi2KG4DynpQ3yWyw0e2qqI11fbaplSpj7ZnapgOiyTb
# l4Vgayibdad5TpOxpHZvmw5OcYauNUK1gOGBh1VmG0M83Nl4Z5RflTWOjaW/j0FT
# nzCjjfX16xTelkDIXMo3oxGrj/qnFkWYucUNlhXz23HNyGj0vR/FD2kQXBU3gLxm
# JkaJ9H4vwVVWPSc7g8B4NQ8GGrGny5GGbcmL5eODGYS4+S6Zi80ecUQXczXIOo4s
# Osu2JqQRPST671Gigh7Aau7KLquW9/QwlMokNCRMKYSNQd5+JP73yZN2eRkDWrcg
# DCSdFfesBvacb3Fa4YhhnM3kcmWt8Z+e0w8eHYft2Jz7OpqUxMS/fHc4AmcMagpp
# AgMBAAGjggIGMIICAjAfBgNVHSMEGDAWgBRoN+Drtjv4XxGG+/5hewiIZfROQjAd
# BgNVHQ4EFgQUZqXdPbubXiqtLH7KbaP0G/WlYHgwDgYDVR0PAQH/BAQDAgeAMBMG
# A1UdJQQMMAoGCCsGAQUFBwMDMIG1BgNVHR8Ega0wgaowU6BRoE+GTWh0dHA6Ly9j
# cmwzLmRpZ2ljZXJ0LmNvbS9EaWdpQ2VydFRydXN0ZWRHNENvZGVTaWduaW5nUlNB
# NDA5NlNIQTM4NDIwMjFDQTEuY3JsMFOgUaBPhk1odHRwOi8vY3JsNC5kaWdpY2Vy
# dC5jb20vRGlnaUNlcnRUcnVzdGVkRzRDb2RlU2lnbmluZ1JTQTQwOTZTSEEzODQy
# MDIxQ0ExLmNybDA+BgNVHSAENzA1MDMGBmeBDAEEATApMCcGCCsGAQUFBwIBFhto
# dHRwOi8vd3d3LmRpZ2ljZXJ0LmNvbS9DUFMwgZQGCCsGAQUFBwEBBIGHMIGEMCQG
# CCsGAQUFBzABhhhodHRwOi8vb2NzcC5kaWdpY2VydC5jb20wXAYIKwYBBQUHMAKG
# UGh0dHA6Ly9jYWNlcnRzLmRpZ2ljZXJ0LmNvbS9EaWdpQ2VydFRydXN0ZWRHNENv
# ZGVTaWduaW5nUlNBNDA5NlNIQTM4NDIwMjFDQTEuY3J0MAwGA1UdEwEB/wQCMAAw
# DQYJKoZIhvcNAQELBQADggIBAGlm35F2ALzbmAAPmi+UUoGnLCpJX0TE3PHXMcR5
# 9mGZHxGfrDkHcWzDK9zGfqTujANHvX8CDRacdytsj87SzAHPP6kC53XNHkh5OPE6
# 1Wb58sqU1rqJVopoWX0NH7BSwk5TDFp5uM44olz+Ra0/h4uo2CD9fBYPgToA8Wkx
# lhSg7/it2osE4MzAJRUVMPwO8AmmJjtKND9FMWG3ceKTwWltDWhsYVbO/uzp7nTq
# FziS2fk0jMKP8ZhWihPotySYU7rvq9hr9qsYzhqJmDIqZrz5VX7WIckTJzA0tOyZ
# udkIEh3y/Nlh6spVVVGRy4sPKGc3v4LchuWPVQ1UGWoZd+uOubgl9diBezCKnDGd
# KvwCaWVYI/5bAzTWNkJURDO2FrpPx7iQOu/Xzds/Y0iLYehuyNAec6EOJNQyd29Q
# gsPFutersW3LwbDVtdqCjf+nk/0pnP2fZd4e7nOg63DS/WQKMFzEAqlnDWA7alAl
# BjyWjhyoiywq0sp0dNUUW6QbO56JnmVKnpA4iE095F5pIDOJkaS1pvcYYQ+RlrsN
# 99tVRjqbiIiBrB1Q0+FER6RyTDa368G19Pdg7jbms1OtZnPzXRlZ4+rNRK7nE22Q
# 5dyxtzlYy/Sv377tK05LxVKlzS3AXTos6Jm66eiiWvZN+wrsEIc7ChLiw+lwVhy0
# ExqYMYIRcDCCEWwCAQEwfTBpMQswCQYDVQQGEwJVUzEXMBUGA1UEChMORGlnaUNl
# cnQsIEluYy4xQTA/BgNVBAMTOERpZ2lDZXJ0IFRydXN0ZWQgRzQgQ29kZSBTaWdu
# aW5nIFJTQTQwOTYgU0hBMzg0IDIwMjEgQ0ExAhAEe51QLiqN2yKyU0tANZXhMA0G
# CWCGSAFlAwQCAQUAoIGEMBgGCisGAQQBgjcCAQwxCjAIoAKAAKECgAAwGQYJKoZI
# hvcNAQkDMQwGCisGAQQBgjcCAQQwHAYKKwYBBAGCNwIBCzEOMAwGCisGAQQBgjcC
# ARUwLwYJKoZIhvcNAQkEMSIEIC0ftcA1JSTajTc3RMnQJ8u60odnZpsM2U4Ml4DM
# WKYkMA0GCSqGSIb3DQEBAQUABIICAE5o3U/dHhXmBZZJBPZqM6lEs77yPQXT1/SD
# CntXcg0gzDeuD2MHDKqueRJvhHNdUdmGY7E8Rjfynr0VUygidg2uyh8GLHnqFjUu
# nD7IP6gB01kUtG57/C/eYzwkmS0zi2ugAG4Q82eAM46NPqzzCg0G2Nh1SOAx/MYT
# bI+LXKL+BAd6V2uOzKU/e9yzOwU6QE0XRqIU/NaqMJuZdXYo5hBIQdbeqfK7RkZT
# Yxfv2AyAg1Y1M3XTcyFw0otv5vcds1AzlP6O5wSLF+xfgE/AehYIL4ilo47BxFe7
# 7ZFdSeDEqcyyGRY9YS0qbF+Z089OQn9AgvW1DmPa35pbeks8v45ecnvdPehFIl3W
# YAIm48a61dgpgp0gYEms2VdT3044Eq3pE9d+czxpQHLyGExb1SNyv+1UCutjnntH
# h4QpcfG9YK6/fOipplvHtNehQxFfWDjgoYUIogDu+PBo9fwCtUFvnSNg9btw/Qqa
# qgrDaAv3kXLUqGv+NRwFDaCLdGiS1ISgkQ4eRdi3TilvF93IGpbeuHjoOqGXkqJL
# sGVKvVxGhCGQ/X/kh2B3GIe+a1pKhn3hH63P9PjjbYIbpEE2q2rMRPLC1XC9UGn0
# riOKP9TEDswHAfFW1uO8NTjz1qBfLC8abrNvpDNBD1DEnig3yl0iHHY1G619Jmjt
# rYCThgOLoYIOPTCCDjkGCisGAQQBgjcDAwExgg4pMIIOJQYJKoZIhvcNAQcCoIIO
# FjCCDhICAQMxDTALBglghkgBZQMEAgEwggEPBgsqhkiG9w0BCRABBKCB/wSB/DCB
# +QIBAQYLYIZIAYb4RQEHFwMwMTANBglghkgBZQMEAgEFAAQgp6Qn8qgNFHQmZXpp
# /eiu/xpNcnRt9PoMQvyqvOcg5/8CFQDImN+7zIu6gh0sjuCpR8jr6qhYpxgPMjAy
# MjA5MDcwNjQzNDhaMAMCAR6ggYakgYMwgYAxCzAJBgNVBAYTAlVTMR0wGwYDVQQK
# ExRTeW1hbnRlYyBDb3Jwb3JhdGlvbjEfMB0GA1UECxMWU3ltYW50ZWMgVHJ1c3Qg
# TmV0d29yazExMC8GA1UEAxMoU3ltYW50ZWMgU0hBMjU2IFRpbWVTdGFtcGluZyBT
# aWduZXIgLSBHM6CCCoswggU4MIIEIKADAgECAhB7BbHUSWhRRPfJidKcGZ0SMA0G
# CSqGSIb3DQEBCwUAMIG9MQswCQYDVQQGEwJVUzEXMBUGA1UEChMOVmVyaVNpZ24s
# IEluYy4xHzAdBgNVBAsTFlZlcmlTaWduIFRydXN0IE5ldHdvcmsxOjA4BgNVBAsT
# MShjKSAyMDA4IFZlcmlTaWduLCBJbmMuIC0gRm9yIGF1dGhvcml6ZWQgdXNlIG9u
# bHkxODA2BgNVBAMTL1ZlcmlTaWduIFVuaXZlcnNhbCBSb290IENlcnRpZmljYXRp
# b24gQXV0aG9yaXR5MB4XDTE2MDExMjAwMDAwMFoXDTMxMDExMTIzNTk1OVowdzEL
# MAkGA1UEBhMCVVMxHTAbBgNVBAoTFFN5bWFudGVjIENvcnBvcmF0aW9uMR8wHQYD
# VQQLExZTeW1hbnRlYyBUcnVzdCBOZXR3b3JrMSgwJgYDVQQDEx9TeW1hbnRlYyBT
# SEEyNTYgVGltZVN0YW1waW5nIENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIB
# CgKCAQEAu1mdWVVPnYxyXRqBoutV87ABrTxxrDKPBWuGmicAMpdqTclkFEspu8LZ
# Kbku7GOz4c8/C1aQ+GIbfuumB+Lef15tQDjUkQbnQXx5HMvLrRu/2JWR8/DubPit
# ljkuf8EnuHg5xYSl7e2vh47Ojcdt6tKYtTofHjmdw/SaqPSE4cTRfHHGBim0P+SD
# DSbDewg+TfkKtzNJ/8o71PWym0vhiJka9cDpMxTW38eA25Hu/rySV3J39M2ozP4J
# 9ZM3vpWIasXc9LFL1M7oCZFftYR5NYp4rBkyjyPBMkEbWQ6pPrHM+dYr77fY5NUd
# bRE6kvaTyZzjSO67Uw7UNpeGeMWhNwIDAQABo4IBdzCCAXMwDgYDVR0PAQH/BAQD
# AgEGMBIGA1UdEwEB/wQIMAYBAf8CAQAwZgYDVR0gBF8wXTBbBgtghkgBhvhFAQcX
# AzBMMCMGCCsGAQUFBwIBFhdodHRwczovL2Quc3ltY2IuY29tL2NwczAlBggrBgEF
# BQcCAjAZGhdodHRwczovL2Quc3ltY2IuY29tL3JwYTAuBggrBgEFBQcBAQQiMCAw
# HgYIKwYBBQUHMAGGEmh0dHA6Ly9zLnN5bWNkLmNvbTA2BgNVHR8ELzAtMCugKaAn
# hiVodHRwOi8vcy5zeW1jYi5jb20vdW5pdmVyc2FsLXJvb3QuY3JsMBMGA1UdJQQM
# MAoGCCsGAQUFBwMIMCgGA1UdEQQhMB+kHTAbMRkwFwYDVQQDExBUaW1lU3RhbXAt
# MjA0OC0zMB0GA1UdDgQWBBSvY9bKo06FcuCnvEHzKaI4f4B1YjAfBgNVHSMEGDAW
# gBS2d/ppSEefUxLVwuoHMnYH0ZcHGTANBgkqhkiG9w0BAQsFAAOCAQEAdeqwLdU0
# GVwyRf4O4dRPpnjBb9fq3dxP86HIgYj3p48V5kApreZd9KLZVmSEcTAq3R5hF2Yg
# VgaYGY1dcfL4l7wJ/RyRR8ni6I0D+8yQL9YKbE4z7Na0k8hMkGNIOUAhxN3WbomY
# PLWYl+ipBrcJyY9TV0GQL+EeTU7cyhB4bEJu8LbF+GFcUvVO9muN90p6vvPN/QPX
# 2fYDqA/jU/cKdezGdS6qZoUEmbf4Blfhxg726K/a7JsYH6q54zoAv86KlMsB257H
# OLsPUqvR45QDYApNoP4nbRQy/D+XQOG/mYnb5DkUvdrk08PqK1qzlVhVBH3Hmuwj
# A42FKtL/rqlhgTCCBUswggQzoAMCAQICEHvU5a+6zAc/oQEjBCJBTRIwDQYJKoZI
# hvcNAQELBQAwdzELMAkGA1UEBhMCVVMxHTAbBgNVBAoTFFN5bWFudGVjIENvcnBv
# cmF0aW9uMR8wHQYDVQQLExZTeW1hbnRlYyBUcnVzdCBOZXR3b3JrMSgwJgYDVQQD
# Ex9TeW1hbnRlYyBTSEEyNTYgVGltZVN0YW1waW5nIENBMB4XDTE3MTIyMzAwMDAw
# MFoXDTI5MDMyMjIzNTk1OVowgYAxCzAJBgNVBAYTAlVTMR0wGwYDVQQKExRTeW1h
# bnRlYyBDb3Jwb3JhdGlvbjEfMB0GA1UECxMWU3ltYW50ZWMgVHJ1c3QgTmV0d29y
# azExMC8GA1UEAxMoU3ltYW50ZWMgU0hBMjU2IFRpbWVTdGFtcGluZyBTaWduZXIg
# LSBHMzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAK8Oiqr43L9pe1QX
# cUcJvY08gfh0FXdnkJz93k4Cnkt29uU2PmXVJCBtMPndHYPpPydKM05tForkjUCN
# Iqq+pwsb0ge2PLUaJCj4G3JRPcgJiCYIOvn6QyN1R3AMs19bjwgdckhXZU2vAjxA
# 9/TdMjiTP+UspvNZI8uA3hNN+RDJqgoYbFVhV9HxAizEtavybCPSnw0PGWythWJp
# /U6FwYpSMatb2Ml0UuNXbCK/VX9vygarP0q3InZl7Ow28paVgSYs/buYqgE4068l
# QJsJU/ApV4VYXuqFSEEhh+XetNMmsntAU1h5jlIxBk2UA0XEzjwD7LcA8joixbRv
# 5e+wipsCAwEAAaOCAccwggHDMAwGA1UdEwEB/wQCMAAwZgYDVR0gBF8wXTBbBgtg
# hkgBhvhFAQcXAzBMMCMGCCsGAQUFBwIBFhdodHRwczovL2Quc3ltY2IuY29tL2Nw
# czAlBggrBgEFBQcCAjAZGhdodHRwczovL2Quc3ltY2IuY29tL3JwYTBABgNVHR8E
# OTA3MDWgM6Axhi9odHRwOi8vdHMtY3JsLndzLnN5bWFudGVjLmNvbS9zaGEyNTYt
# dHNzLWNhLmNybDAWBgNVHSUBAf8EDDAKBggrBgEFBQcDCDAOBgNVHQ8BAf8EBAMC
# B4AwdwYIKwYBBQUHAQEEazBpMCoGCCsGAQUFBzABhh5odHRwOi8vdHMtb2NzcC53
# cy5zeW1hbnRlYy5jb20wOwYIKwYBBQUHMAKGL2h0dHA6Ly90cy1haWEud3Muc3lt
# YW50ZWMuY29tL3NoYTI1Ni10c3MtY2EuY2VyMCgGA1UdEQQhMB+kHTAbMRkwFwYD
# VQQDExBUaW1lU3RhbXAtMjA0OC02MB0GA1UdDgQWBBSlEwGpn4XMG24WHl87Map5
# NgB7HTAfBgNVHSMEGDAWgBSvY9bKo06FcuCnvEHzKaI4f4B1YjANBgkqhkiG9w0B
# AQsFAAOCAQEARp6v8LiiX6KZSM+oJ0shzbK5pnJwYy/jVSl7OUZO535lBliLvFeK
# kg0I2BC6NiT6Cnv7O9Niv0qUFeaC24pUbf8o/mfPcT/mMwnZolkQ9B5K/mXM3tRr
# 41IpdQBKK6XMy5voqU33tBdZkkHDtz+G5vbAf0Q8RlwXWuOkO9VpJtUhfeGAZ35i
# rLdOLhWa5Zwjr1sR6nGpQfkNeTipoQ3PtLHaPpp6xyLFdM3fRwmGxPyRJbIblumF
# COjd6nRgbmClVnoNyERY3Ob5SBSe5b/eAL13sZgUchQk38cRLB8AP8NLFMZnHMwe
# BqOQX1xUiz7jM1uCD8W3hgJOcZ/pZkU/djGCAlowggJWAgEBMIGLMHcxCzAJBgNV
# BAYTAlVTMR0wGwYDVQQKExRTeW1hbnRlYyBDb3Jwb3JhdGlvbjEfMB0GA1UECxMW
# U3ltYW50ZWMgVHJ1c3QgTmV0d29yazEoMCYGA1UEAxMfU3ltYW50ZWMgU0hBMjU2
# IFRpbWVTdGFtcGluZyBDQQIQe9Tlr7rMBz+hASMEIkFNEjALBglghkgBZQMEAgGg
# gaQwGgYJKoZIhvcNAQkDMQ0GCyqGSIb3DQEJEAEEMBwGCSqGSIb3DQEJBTEPFw0y
# MjA5MDcwNjQzNDhaMC8GCSqGSIb3DQEJBDEiBCCkg1sDkYD47Pp+LDJifw2zEDSL
# IrkGUEyRRpUVV4JuJDA3BgsqhkiG9w0BCRACLzEoMCYwJDAiBCDEdM52AH0COU4N
# peTefBTGgPniggE8/vZT7123H99h+DALBgkqhkiG9w0BAQEEggEAVvMsTxvaCTGf
# j+n6c6YBbvHyRlTPN+BimvtCML4qVJ4y8lSfTnPCXvW7+d32cJNNL0GzHRU5jwHt
# bK8GtkxGpRQzYxPsl2vCT+BQpneyGVpCe1mR78ceauMNneMt4purGJbUCU/mgV+U
# hdSpgb4LsOiQ82qt/kKVpUQZSM949gtb32aoe4WwVcpYHtx9MJBs3wa7khEVAuGu
# oHre3/8+RLp8aWyfmfKo6E40FxfBqqwVe8f1KJdxcUedzLPpEyk3Bp4/7mevQvwC
# dV9IUrHENaGJi6teKGy462+a9gE9Ib2RWjqkCWRvuRoIYCyuU5NpKXOVZgbH7h2u
# DcCQlBaO6A==
# SIG # End signature block
