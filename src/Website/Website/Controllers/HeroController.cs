﻿using Sitecore.Data.Fields;
using Sitecore.Links;
using Sitecore.Mvc.Controllers;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models.Components;

namespace Website.Controllers
{
    public class HeroController : SitecoreController
    {
        // GET: Hero
        public ActionResult Hero()
        {
            var dataSourceItem = RenderingContext.Current.Rendering.Item;
            var viewModel = new Hero
            {
                ID = dataSourceItem.ID,
                Title = new HtmlString(FieldRenderer.Render(dataSourceItem, "Title")),
                Description = new HtmlString(FieldRenderer.Render(dataSourceItem, "Description")),
                Image = new HtmlString(FieldRenderer.Render(dataSourceItem, "Image")),
                sourceItem = dataSourceItem
            };

            ImageField imgField = dataSourceItem.Fields["Image"];
            if (imgField != null && imgField.MediaItem != null) {
                viewModel.ImageUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
            }
            LinkField linkField = dataSourceItem.Fields["Link"];
            if (linkField != null)
            {
                viewModel.Link = linkField;
            }
            return View(viewModel);
        }
    }
}