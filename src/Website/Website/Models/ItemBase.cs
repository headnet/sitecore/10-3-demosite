﻿using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class ItemBase
    {
        public ID ID { get; set; }
        public Item sourceItem { get; set; }
    }
}