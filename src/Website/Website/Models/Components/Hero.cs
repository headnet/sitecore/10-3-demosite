﻿using Sitecore.Data.Fields;
using Sitecore.Mvc.Presentation;
using Sitecore.Shell.Feeds.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Models.Components
{
    public class Hero : ItemBase
    {
        // Fields
        public HtmlString Title { get; set; }
        public HtmlString Description { get; set; }
        public HtmlString Image { get; set; }

        // Props
        public string ImageUrl { get; set; }
        public LinkField Link { get; set; }


    }
}