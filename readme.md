# Opsætning af lokalt udviklingsmiljø

## Indledende øvelser
Download zip fil fra https://sitecoredev.azureedge.net/~/media/F822AC26C1AC4805BA78E36E82545093.ashx?date=20221124T084005.
Sørg for at du har en gyldig licensfil klar.

## 1. Gør klar til at køre SIF (Sitecore Installation Framework).
### Juster execution policy om nødvendigt
Åben powershell som administrator.
Tjek execution policy med kommandoen
```
Get-ExecutionPolicy
```

hvis den er resricted kan du ændre til f.ek.s remote signed med

```
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
```

eller give fuld adgang med

```
Set-ExecutionPolicy -ExecutionPolicy Unrestricted
```

### Hent Installationsfiler til Sitecore
Det er version 10.3 i XM versionen vi skal bruge.

Sitecore hentes som en zip-fil her:
https://sitecoredev.azureedge.net/~/media/F822AC26C1AC4805BA78E36E82545093.ashx?date=20221124T084005

Filen indeholder 4 pakker med konfigurationsfiler til de forskellige sitecore miljøer. Filerne pakkes ud og placeres i ``.\Install``. 

### Placér Licens
Placer din sitecore licensfil (license.xml) under ``.\Install``.

## 2. Installér SIF

Sitecore SIF er et powershell bibliotek, som findes i Sitecores eget NuGet-repositorie "SitecoreGallery".

### Tilføj SitecoreGallery som Nuget kilde.
```
Register-PSRepository -Name SitecoreGallery -SourceLocation https://sitecore.myget.org/F/sc-powershell/api/v2
```

Når SitecoreGallery er installeret. Man kan tjekke sitecore-gallery installationen med kommandoen ``Get-PSRepository`` som bør give
```
PS C:\> Get-PSRepository
Name                      InstallationPolicy   SourceLocation
----                      ------------------   --------------
PSGallery                 Untrusted            https://www.powershellgallery.com/api/v2/
SitecoreGallery           Untrusted            https://sitecore.myget.org/F/sc-powershell/api/v2
```

(NB: Hvis PSGallery mangler kan man installere det med kommandoen ``Register-PSRepository -Default``.)

Hvis ikke sourcelocation er korrekt kan man ændre den med
`Set-PSRepository -Name SitecoreGallery -SourceLocation https://sitecore.myget.org/F/sc-powershell/api/v2`.

### Installér SIF powershell-modulet.
```
Install-Module SitecoreInstallFramework
```
er modulet allerede installeret bør man til seneste version med kommandoen
```
Update-Module SitecoreInstallFramework
```
Man kan se hvilke versioner af SIF der er installeret med kommandoen
```
Get-Module SitecoreInstallFramework –ListAvailable
```

Man skal bruge forskellig version af SIF afhængig af hvilken version af sitecore man vil deploye

| Sitecore Experience Platform | Version SIF Version |
| 9.0.X | 1.2.1 |
| 9.1.0 | 2.0.0 |
| 9.1.1 | 2.1.0 or later |
| 9.2.0 | 2.1.0 or later |
| 9.3.0 | 2.2.0 |
| 10.X.X | 2.3.0 |

Man kan installere en specifik verison af SIF med kommandoen
```
Install-Module -Name SitecoreInstallFramework -RequiredVersion x.x.x
```

og kører en specifik version af SIF for sin powershell session med kommandoen
```
Import-Module -Name SitecoreInstallFramework -Force -RequiredVersion x.x.x
```

NB:
Selve SIF powershell filerne for de forskellige versioner kan findes i 
`C:\Program Files\WindowsPowerShell\Modules\SitecoreInstallFramework`.




## 3. Installér Solr og MsSql via docker

Vi kører `solr` og `mssql` via Docker for at undgå konflikter med andre lokale installationer. Begge skal være installeret og startet før selve sitecore-installationen kan køre. 


```
cd .\Install\10-3-demo-docker
docker-compose up -d
```

SqlServer starter nu op på localhost,14123.

Solr er tilgængelig på http://localhost:8987/solr
    
Solr er som standard blevet oprettet med følgende cores  

```
Sitecore103Demo_core_index Sitecore103Demo_web_index Sitecore103Demo_master_index
```

*Bemærk:* for at ændre navn på installationen ændres `$Prefix` i `Xm1-Sitecore103Demo.ps1`. core-navne i solr skal rettes tilsvarende i `.\Install\10-3-demo-docker\solr\Dockerfile`.


>*Kursorisk*: Proceduren for at installere uden brug af docker er blot at justere variablerne for `$SolrUrl` og `$SqlServer` og køre SIF-installationen med standard-konfigurationen `XM1-SingleDeveloper.json` i stedet for `Xm1-Sitecore103Demo.ps1`. 




## 4. Installer et lokalt Sitecore miljø med SIF
Nu er SIF installeret og kan bruges til at installere alverdens ting via nogle tasks defineret i en config-fil (man kan læse mere i [Sitecore_Installation_Framework_Configuration_Guide-2.3.0.pdf](Sitecore_Installation_Framework_Configuration_Guide-2.3.0.pdf)).

### Installér nødvendige *Prerequisites*
Første ting vi skal have installeret er andre prerequisites det kan f.eks. være MS Deploy og lignende som også skal bruges til installationen.<br/>
Der findes i sitecore's installationspakke en konfigurationsfil til dette kaldet `Prerequisites.json`<br/>
Desværre er der en fejl i denne, fordi man ikke længere kan installere MSDeploy og UrlRewriter modulerne via microsoft's webplatform installer.<br/>
Vi har derfor tilføjet en ekstra prerequisite konfigurationsfil "UrlRewriteAndMsDeploy.json" som skal køres først<br/>
<br/>
Hvis du står i `.\Install` folderen i repositoriet, så vil installation af prerequisites foregår med kommandoen
```
Install-SitecoreConfiguration –Path ".\UrlRewriteAndMsDeploy.json"
```
og derefter<br/>
```
Install-SitecoreConfiguration –Path ".\Prerequisites.json"
```
Installationen afsluttes med følgende besked. Luk dit PowerShell-vindue og start et nyt - stadig som Administarator.

>Sitecore prerequisites are now installed, **YOU MUST** launch a new PowerShell session to run further SIF configurations.


### Installér Sitecore 

Installationen af selve Sitecore kører også via SIF. 
Til lokal opsætning af dette demo projekt er oprettet en custom SIF-konfiguration og et custom PowerShell script.

- **[XM1-Sitecore103Demo.json](Install/XM1-Sitecore103Demo.json)** er kopi af  [XM1-SingleDeveloper.json](Install/XM1-SingleDeveloper.json), der undlader installation af `solr`og `mssql`. Disse kører vi i stedet via docker.
- **[Xm1-Sitecore103Demo.ps1](Install/Xm1-Sitecore103Demo.ps1)** er et powershell script, der sætter de rette variabler og derefter kalder `Install-SitecoreConfiguration –Path ".\XM1-Sitecore103Demo.json`. 

Tjek at variabler i [Xm1-Sitecore103Demo.ps1](Install/Xm1-Sitecore103Demo.ps1) passer til dit lokale miljø og kør derefter scriptet med (især "$SCInstallRoot")

```
.\Xm1-Sitecore103Demo.ps1
```

Efter installationen har du nu 
 - En installation af IdentityServer tilgængeligt på https://sitecore103demo.identityserver
 - En installation af Sitecore CM tilgængeligt på https://sitecore103demo.cm
 - Certifikater til begge

NB: If you get a SMO error then you may be missing "Microsoft SQL Server 2016 Management Objects".
A workaround here can be to just install MSSQL 2016

## Tillykke! Dit lokale sitecore miljø er nu oppe at køre.
=======

https://sitecore103demo.cm

# Opsætning af Sitecore web solution
Følg vejledning fra: https://doc.sitecore.com/xp/en/developers/103/sitecore-experience-manager/set-up-sitecore-and-visual-studio-for-development.html
NB: Husk at åbne visual studio i admin mode ellers kan publicering fejler